
//routing di angular
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/myAccount', {
       templateUrl: 'myAccount.html', controller: 'myAccountController'
    })
    .when('/registrazione', {
      templateUrl: 'registrazione.html', controller: 'registraController'
   })    
   .when('/statistiche', {
      templateUrl: 'statistiche.html', controller: 'statController'
   })
    .otherwise ({
       redirectTo: '/index.html'
    });
    
 }]);
 //Controller da modificare
 app.controller('myAccountController', function($scope) {
    $scope.message = "vediamo se porcoddio funziona";
 });
 app.controller('registraController', function($scope) {
   $scope.message = "vediamo se porcoddio funziona";
});
app.controller('statController', function($scope,$http) {
   $http.get("weeklystat.php")
   .then(function(response){$scope.stat = response.data.records;});
   
});
