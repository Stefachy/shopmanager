new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: ["Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "domenica"],
      datasets: [
        {
          label: "Spesa (Euro)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [$scope.stat.lun,$scope.stat.mar,$scope.stat.mer,$scope.stat.gio,$scope.stat.ven,$scope.stat.sab,$scope.stat.dom]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Spesa Settimanale'
      }
    }
});